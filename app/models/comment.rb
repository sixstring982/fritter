class Comment < ActiveRecord::Base

  acts_as_taggable
  
  belongs_to :post
  belongs_to :user

  validates :commenter, presence: true
  validates :body, presence: true
end
