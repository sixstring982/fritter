class Post < ActiveRecord::Base

  acts_as_taggable

  has_many :comments, dependent: :destroy
  belongs_to :user

  validates :title, presence: true,
  length: {minimum: 5}

  validates :owner, presence: true,
  length: {minimum: 5}

end
