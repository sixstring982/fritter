class CommentsController < ApplicationController

  before_action :get_idx_post, only: [:create, :destroy]
  before_action :get_idx_comment, only: [:destroy]

  def index
    @comments = Comment.all
  end

  def destroy
    @comment.destroy
    redirect_to post_path(@post)
  end

  def create
    @comment = 
      @post.comments.create(comment_params)
    @comment.save
    redirect_to post_path(@post)
  end

  private
  
    def comment_params
      params[:comment].permit(:commenter, :body)
    end

    def get_idx_post
      @post = Post.find(params[:post_id])
    end

    def get_idx_comment
      @comment = @post.comments.find(params[:id])
    end
  
end
