class PostsController < ApplicationController

  before_action :get_idx_post, :only => [:show, :edit, :update, :destroy]

  def show
    @post = Post.find(params[:id])
  end

  def new
    if not user_signed_in?
      redirect_to new_user_session_path
    else
      @post = Post.new
    end
  end

  def create
    @post = Post.new(post_params)
    @post.owner = current_user.email
    @post.user_id = current_user.id
    @post.tag_list = filter_hashtags(@post.text)
    if @post.save
        redirect_to post_path(@post)
    else
      render 'new'
    end
  end

  def destroy
    @post.destroy
    redirect_to posts_path
  end

  def edit
  end

  def index
    @posts = Post.all.reverse #Order them from new -> old
  end

  def update
    if @post.update(post_params)
      redirect_to post_path(@post)
      tags = filter_hashtags(@post.text)
      if not tags.nil? || tags.empty?
        @post.tag_list = tags
        @post.save
      end
    else
      render 'edit'
    end
  end

  private
    def get_idx_post
      @post = Post.find(params[:id])
    end

    def post_params
      params.require(:post).permit(:title, :text)
    end
    
    def filter_hashtags(text)
      (text.split.select{|w| w.starts_with?('#')}).uniq
    end

end
