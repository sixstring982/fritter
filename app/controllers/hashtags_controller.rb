class HashtagsController < ApplicationController

  before_action :get_posts_by_comments
  before_action :get_tag_param, only: [:show]

  def index
    @posts = Post.all(:order => "created_at").select{|p| p.tag_list.count > 0}
  end

  def show
    @posts = @posts.select{|p| p.tag_list.include? @tag }
  end

  private
    def get_posts_by_comments
      @posts = Post.all :joins => :comments, :group => 'posts.id', :order => 'count(*)'
      @posts = (@posts + Post.all.select{|p| p.comments.count == 0}).take(10)
    end

    def get_tag_param
      @tag = params[:id]
    end

end
