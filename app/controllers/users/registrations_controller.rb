class Users::RegistrationsController < Devise::RegistrationsController

  def new
    super
  end

  def update
    super
  end

  def trim_username(username)
    username[0, username.index('@')]
  end

  def create
    super
    @user.username = trim_username params[:user].require(:email)
    @user.save
  end

  def posts
    @user = User.find(params[:id])
    @posts = @user.posts
  end

end
