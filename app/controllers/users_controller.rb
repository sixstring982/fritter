class UsersController < ApplicationController

  def show
    @user = User.where(username: params[:id])[0]
    @posts = @user.posts
  end

  def posts
    @user = User.where(username: params[:id])[0]
    @posts = @user.posts
  end

end
