class StatisticsController < ApplicationController

  def index
    @posts     = Post.all(:joins => :comments, :group => 'posts.id', :order => 'count(*)')
    @users     = User.all(:joins => :posts, :group => 'users.id', :order => 'count(*)')
    tags      = get_hashtags(@posts + Post.all.select{|p| p.comments.count == 0})
    utags = tags.uniq
    @tagcounts = utags.zip(utags.map{|tag| tags.count(tag)})
  end

  private
    def get_hashtags(posts)
      htags = []
      posts.each do |post|
        post.tag_list.each do |tag|
          htags.push(tag)
        end
      end
      return htags.sort{|x, y| htags.count(x) <=> htags.count(y)}
    end
end
